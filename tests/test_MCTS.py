import sys

sys.path.append("..")

from src import engine
from src import MCTS
import numpy as np


def assert_almost_equal(actual, expected, delta=0.001):
    assert abs(actual - expected) < delta


def test_find_moves_1():
    assert MCTS.find_moves(engine.State()) == [
        (0, (0, 0)),
        (0, (0, 1)),
        (0, (0, 2)),
        (0, (0, 3)),
        (0, (0, 4)),
        (0, (0, 5)),
        (0, (0, 6)),
        (0, (0, 7)),
        (0, (1, 0)),
        (0, (1, 1)),
        (0, (1, 2)),
        (0, (1, 3)),
        (0, (1, 4)),
        (0, (1, 5)),
        (0, (1, 6)),
        (0, (1, 7)),
        (0, (2, 0)),
        (0, (2, 1)),
        (0, (2, 2)),
        (0, (2, 3)),
        (0, (2, 4)),
        (0, (2, 5)),
        (0, (2, 6)),
        (0, (2, 7)),
        (0, (3, 0)),
        (0, (3, 1)),
        (0, (3, 2)),
        (0, (3, 3)),
        (0, (3, 4)),
        (0, (3, 5)),
        (0, (3, 6)),
        (0, (3, 7)),
        (0, (4, 0)),
        (0, (4, 1)),
        (0, (4, 2)),
        (0, (4, 3)),
        (0, (4, 4)),
        (0, (4, 5)),
        (0, (4, 6)),
        (0, (4, 7)),
        (0, (5, 0)),
        (0, (5, 1)),
        (0, (5, 2)),
        (0, (5, 3)),
        (0, (5, 4)),
        (0, (5, 5)),
        (0, (5, 6)),
        (0, (5, 7)),
        (0, (6, 0)),
        (0, (6, 1)),
        (0, (6, 2)),
        (0, (6, 3)),
        (0, (6, 4)),
        (0, (6, 5)),
        (0, (6, 6)),
        (0, (6, 7)),
        (0, (7, 0)),
        (0, (7, 1)),
        (0, (7, 2)),
        (0, (7, 3)),
        (0, (7, 4)),
        (0, (7, 5)),
        (0, (7, 6)),
        (0, (7, 7)),
        (1, (0, 0)),
        (1, (0, 1)),
        (1, (0, 2)),
        (1, (0, 3)),
        (1, (0, 4)),
        (1, (0, 5)),
        (1, (0, 6)),
        (1, (0, 7)),
        (1, (1, 0)),
        (1, (1, 1)),
        (1, (1, 2)),
        (1, (1, 3)),
        (1, (1, 4)),
        (1, (1, 5)),
        (1, (1, 6)),
        (1, (1, 7)),
        (1, (2, 0)),
        (1, (2, 1)),
        (1, (2, 2)),
        (1, (2, 3)),
        (1, (2, 4)),
        (1, (2, 5)),
        (1, (2, 6)),
        (1, (2, 7)),
        (1, (3, 0)),
        (1, (3, 1)),
        (1, (3, 2)),
        (1, (3, 3)),
        (1, (3, 4)),
        (1, (3, 5)),
        (1, (3, 6)),
        (1, (3, 7)),
        (1, (4, 0)),
        (1, (4, 1)),
        (1, (4, 2)),
        (1, (4, 3)),
        (1, (4, 4)),
        (1, (4, 5)),
        (1, (4, 6)),
        (1, (4, 7)),
        (1, (5, 0)),
        (1, (5, 1)),
        (1, (5, 2)),
        (1, (5, 3)),
        (1, (5, 4)),
        (1, (5, 5)),
        (1, (5, 6)),
        (1, (5, 7)),
        (1, (6, 0)),
        (1, (6, 1)),
        (1, (6, 2)),
        (1, (6, 3)),
        (1, (6, 4)),
        (1, (6, 5)),
        (1, (6, 6)),
        (1, (6, 7)),
        (1, (7, 0)),
        (1, (7, 1)),
        (1, (7, 2)),
        (1, (7, 3)),
        (1, (7, 4)),
        (1, (7, 5)),
        (1, (7, 6)),
        (1, (7, 7)),
    ]


def test_find_moves_2():
    state = engine.State(
        board=np.array(
            [
                [2, 2, 3, 0, 1, 0, 0, 0],
                [2, 2, 3, 0, 1, 0, 0, 0],
                [2, 3, 3, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [1, 1, 1, 1, 0, 1, 1, 1],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 3],
            ],
            dtype=np.int8,
        ),
        moves_count=24,
        num_own_stones=[0, 0],
    )
    assert MCTS.find_moves(state) == [
        (0, (0, 3)),
        (0, (0, 5)),
        (0, (0, 6)),
        (0, (0, 7)),
        (0, (1, 3)),
        (0, (1, 5)),
        (0, (1, 6)),
        (0, (1, 7)),
        (0, (2, 3)),
        (0, (2, 5)),
        (0, (2, 6)),
        (0, (2, 7)),
        (0, (3, 0)),
        (0, (3, 1)),
        (0, (3, 2)),
        (0, (3, 3)),
        (0, (3, 5)),
        (0, (3, 6)),
        (0, (3, 7)),
        # (0, (4, 4)),
        (0, (5, 0)),
        (0, (5, 1)),
        (0, (5, 2)),
        (0, (5, 3)),
        (0, (5, 5)),
        (0, (5, 6)),
        (0, (5, 7)),
        (0, (6, 0)),
        (0, (6, 1)),
        (0, (6, 2)),
        (0, (6, 3)),
        (0, (6, 5)),
        (0, (6, 6)),
        (0, (6, 7)),
        (0, (7, 0)),
        (0, (7, 1)),
        (0, (7, 2)),
        (0, (7, 3)),
        (0, (7, 5)),
        (0, (7, 6)),
    ]


def test_find_moves_3():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=14,
        num_own_stones=[0, 0],
    )
    assert MCTS.find_moves(state) == [
        (0, (0, 0)),
        (0, (0, 1)),
        (0, (0, 2)),
        (0, (0, 3)),
        (0, (0, 5)),
        (0, (0, 6)),
        (0, (0, 7)),
        (0, (1, 0)),
        (0, (1, 1)),
        (0, (1, 2)),
        (0, (1, 3)),
        (0, (1, 5)),
        (0, (1, 6)),
        (0, (1, 7)),
        (0, (2, 0)),
        (0, (2, 1)),
        (0, (2, 2)),
        (0, (2, 3)),
        (0, (2, 5)),
        (0, (2, 6)),
        (0, (2, 7)),
        (0, (3, 0)),
        (0, (3, 1)),
        (0, (3, 2)),
        (0, (3, 3)),
        (0, (3, 5)),
        (0, (3, 6)),
        (0, (3, 7)),
        (0, (5, 0)),
        (0, (5, 1)),
        (0, (5, 2)),
        (0, (5, 3)),
        (0, (5, 5)),
        (0, (5, 6)),
        (0, (5, 7)),
        (0, (6, 0)),
        (0, (6, 1)),
        (0, (6, 2)),
        (0, (6, 3)),
        (0, (6, 5)),
        (0, (6, 6)),
        (0, (6, 7)),
        (0, (7, 0)),
        (0, (7, 1)),
        (0, (7, 2)),
        (0, (7, 3)),
        (0, (7, 5)),
        (0, (7, 6)),
        (0, (7, 7)),
    ]


def test_find_moves_4():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 3, 2, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 3, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=11,
        num_own_stones=[2, 2],
    )
    assert MCTS.find_moves(state) == []


def test_find_moves_5():
    state = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 2, 1, 0],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [3, 3, 3, 3, 3, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=59,
        num_own_stones=[0, 0],
    )
    moves = MCTS.find_moves(state)
    assert moves == [(0, (0, 7)), (0, (7, 0))]


def test_find_moves_6():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 3, 3, 1, 0, 0, 1],
                [1, 0, 0, 1, 3, 0, 1, 1],
                [0, 1, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 1, 3, 0],
                [0, 1, 2, 1, 0, 0, 1, 2],
                [0, 0, 0, 0, 1, 1, 2, 0],
                [0, 1, 0, 0, 0, 1, 1, 0],
                [0, 2, 0, 0, 3, 1, 0, 1],
            ],
            dtype=np.int8,
        ),
        moves_count=29,
        num_own_stones=[0, 0],
    )
    moves = MCTS.find_moves(state)
    assert len(moves) == 35


def test_find_moves_7():
    state = engine.State(
        board=np.array(
            [[3, 2, 1, 3], [0, 1, 3, 2], [1, 0, 1, 1], [2, 1, 1, 1]], dtype=np.int8
        ),
        moves_count=14,
        num_own_stones=[0, 0],
    )
    moves = MCTS.find_moves(state)
    assert moves == []


def test_relative_reward_1():
    node = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=14,
        num_own_stones=[0, 0],
    )
    child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 1, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=15,
        num_own_stones=[0, 0],
    )
    grand_child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 1, 0],
                [0, 0, 0, 0, 1, 0, 0, 1],
            ],
            dtype=np.int8,
        ),
        moves_count=16,
        num_own_stones=[0, 0],
    )
    mcts = MCTS.MCTS()
    mcts.visits[child] = 10
    mcts.rewards[child] = 3
    mcts.visits[grand_child] = 11
    mcts.rewards[grand_child] = 3
    assert mcts.relative_reward(child) == 3
    assert mcts.relative_reward(grand_child) == 8


def test_select_1():
    mcts = MCTS.MCTS()
    node = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=14,
        num_own_stones=[0, 0],
    )
    child_1 = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 1, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=15,
        num_own_stones=[0, 0],
    )
    child_2 = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 1, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=15,
        num_own_stones=[0, 0],
    )
    grandchild = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 1, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 1, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=16,
        num_own_stones=[0, 0],
    )
    mcts.children[node].append(child_2)
    mcts.children[node].append(child_1)
    mcts.children[child_1].append(grandchild)
    mcts.unexpanded_moves[node] = []
    mcts.unexpanded_moves[child_1] = []
    mcts.unexpanded_moves[child_2] = []
    mcts.unexpanded_moves[grandchild] = [(0, (2, 2))]
    mcts.visits[node] = 19
    mcts.rewards[child_1] = 3.5
    mcts.visits[child_1] = 11
    mcts.rewards[child_2] = 4
    mcts.visits[child_2] = 14
    mcts.rewards[grandchild] = 4
    mcts.visits[grandchild] = 12

    assert_almost_equal(mcts.ucb_score(node, child_1), 0.8355562192042, delta=0.001)
    assert_almost_equal(mcts.ucb_score(node, child_2), 0.7443176763913, delta=0.001)
    # assert_almost_equal(
    #    mcts.ucb_score(child_1, grandchild), 0.8286816097314, delta=0.001
    # )
    assert mcts.choose(node, mcts.ucb_score) == child_1
    assert mcts.choose(child_1, mcts.ucb_score) == grandchild
    assert mcts.select(node) == [node, child_1, grandchild]
    assert mcts.select(child_1) == [child_1, grandchild]
    assert mcts.select(grandchild) == [grandchild]
    assert mcts.select(child_2) == [child_2]
    mcts.rewards[child_2] = 10
    assert_almost_equal(mcts.ucb_score(node, child_2), 1.1728891049627, delta=0.001)
    assert mcts.select(node) == [node, child_2]


def test_select_2():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [3, 3, 3, 3, 0, 3, 1, 1],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=14,
        num_own_stones=[0, 0],
    )
    assert mcts.select(state) == [state]
    assert mcts.unexpanded_moves[state] == [
        (0, (0, 0)),
        (0, (0, 1)),
        (0, (0, 2)),
        (0, (0, 3)),
        (0, (0, 5)),
        (0, (0, 6)),
        (0, (0, 7)),
        (0, (1, 0)),
        (0, (1, 1)),
        (0, (1, 2)),
        (0, (1, 3)),
        (0, (1, 5)),
        (0, (1, 6)),
        (0, (1, 7)),
        (0, (2, 0)),
        (0, (2, 1)),
        (0, (2, 2)),
        (0, (2, 3)),
        (0, (2, 5)),
        (0, (2, 6)),
        (0, (2, 7)),
        (0, (3, 0)),
        (0, (3, 1)),
        (0, (3, 2)),
        (0, (3, 3)),
        (0, (3, 5)),
        (0, (3, 6)),
        (0, (3, 7)),
        (0, (5, 0)),
        (0, (5, 1)),
        (0, (5, 2)),
        (0, (5, 3)),
        (0, (5, 5)),
        (0, (5, 6)),
        (0, (5, 7)),
        (0, (6, 0)),
        (0, (6, 1)),
        (0, (6, 2)),
        (0, (6, 3)),
        (0, (6, 5)),
        (0, (6, 6)),
        (0, (6, 7)),
        (0, (7, 0)),
        (0, (7, 1)),
        (0, (7, 2)),
        (0, (7, 3)),
        (0, (7, 5)),
        (0, (7, 6)),
        (0, (7, 7)),
    ]


def test_select_3():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [3, 1, 1, 3, 1, 3, 1, 3],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=11,
        num_own_stones=[2, 1],
    )
    assert mcts.select(state) == [state]
    assert mcts.unexpanded_moves[state] == []


def test_ucb_score_1():
    node = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 3, 2, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 3, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=11,
        num_own_stones=[2, 2],
    )
    child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 3, 2, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 3, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 1, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=12,
        num_own_stones=[2, 2],
    )
    grand_child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 3, 2, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 3, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 1, 0],
                [0, 0, 0, 0, 1, 0, 0, 1],
            ],
            dtype=np.int8,
        ),
        moves_count=13,
        num_own_stones=[2, 2],
    )
    mcts = MCTS.MCTS()
    mcts.visits[node] = 11
    mcts.rewards[node] = 8
    mcts.visits[child] = 7
    mcts.rewards[child] = 6
    mcts.visits[grand_child] = 5
    mcts.rewards[grand_child] = 4
    assert mcts.relative_reward(node) == 8
    assert mcts.relative_reward(child) == 1
    assert mcts.relative_reward(grand_child) == 4
    assert_almost_equal(mcts.ucb_score(node, child), 0.7281403798823, delta=0.001)
    assert_almost_equal(
        mcts.ucb_score(child, grand_child), 1.4238445558078, delta=0.001
    )


def test_pop_random_1():
    my_list = [1, 2, 3, 5]
    MCTS.pop_random(my_list)
    assert len(my_list) == 3
    MCTS.pop_random(my_list)
    assert len(my_list) == 2
    MCTS.pop_random(my_list)
    assert len(my_list) == 1
    MCTS.pop_random(my_list)
    assert len(my_list) == 0


def test_expand_1():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [3, 3, 3, 3, 3, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=60,
        num_own_stones=[0, 0],
    )
    mcts.expand([state])
    assert mcts.unexpanded_moves[state] == []
    assert mcts.children[state] == [
        engine.State(
            board=np.array(
                [
                    [1, 1, 1, 1, 1, 2, 1, 1],
                    [1, 1, 1, 1, 1, 2, 1, 1],
                    [1, 1, 1, 1, 1, 2, 1, 1],
                    [1, 1, 1, 1, 1, 2, 1, 1],
                    [1, 1, 1, 1, 1, 2, 1, 1],
                    [3, 3, 3, 3, 3, 0, 1, 1],
                    [1, 1, 1, 1, 1, 1, 0, 1],
                    [1, 1, 1, 1, 1, 1, 1, 0],
                ],
                dtype=np.int8,
            ),
            moves_count=61,
            num_own_stones=[0, 0],
        )
    ]
    assert mcts.unexpanded_moves[mcts.children[state][0]] == []
    assert mcts.children[mcts.children[state][0]] == []


def test_expand_2():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 2, 1, 1, 1],
                [1, 1, 1, 1, 2, 1, 1, 1],
                [1, 1, 1, 1, 2, 1, 1, 1],
                [1, 1, 1, 1, 2, 1, 1, 1],
                [3, 3, 3, 3, 0, 1, 1, 1],
                [1, 1, 1, 1, 1, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=59,
        num_own_stones=[1, 1],
    )
    assert len(mcts.unexpanded_moves[state]) == 6
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 5
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 4
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 3
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 2
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 1
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 0
    mcts.expand([state])
    assert len(mcts.unexpanded_moves[state]) == 0
    child = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 2, 1, 1, 1],
                [1, 1, 1, 1, 2, 1, 1, 1],
                [1, 1, 1, 1, 2, 1, 1, 1],
                [1, 1, 1, 1, 2, 1, 1, 1],
                [3, 3, 3, 3, 0, 1, 1, 1],
                [1, 1, 1, 1, 1, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [3, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=60,
        num_own_stones=[1, 0],
    )
    assert child in mcts.children[state]


def test_expand_3():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=0,
        num_own_stones=[5, 5],
    )
    child_1 = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 2, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=1,
        num_own_stones=[4, 5],
    )
    child_2 = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 3, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=1,
        num_own_stones=[4, 5],
    )
    for _ in range(150):
        mcts.expand([state])
    assert child_1 in mcts.children[state]
    assert child_2 not in mcts.children[state]


def test_evaluate_1():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [2, 2, 2, 2, 1, 0, 1, 1],
                [3, 3, 3, 3, 3, 1, 0, 1],
                [2, 0, 0, 0, 0, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=17,
        num_own_stones=[0, 0],
    )
    for _ in range(10):
        assert mcts.evaluate(state) == 0


def test_evaluate_2():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [1, 1, 0, 0, 0, 0, 0, 0],
                [1, 0, 1, 0, 0, 0, 0, 0],
                [0, 1, 1, 0, 0, 0, 0, 0],
                [1, 2, 0, 0, 0, 0, 0, 0],
                [3, 2, 0, 0, 0, 0, 0, 0],
                [3, 2, 0, 0, 0, 0, 0, 0],
                [3, 2, 0, 0, 0, 0, 0, 0],
                [3, 2, 3, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=17,
        num_own_stones=[0, 0],
    )
    for _ in range(10):
        assert mcts.evaluate(state) == 1


def test_backpropagate_1():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=0,
        num_own_stones=[5, 5],
    )
    child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 2, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=1,
        num_own_stones=[4, 5],
    )
    grand_child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 2, 0, 0, 0, 0],
                [0, 0, 0, 0, 3, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=2,
        num_own_stones=[4, 4],
    )
    mcts.backpropagate(1, [state, child, grand_child])
    assert mcts.visits[state] == 1
    assert mcts.visits[child] == 1
    assert mcts.visits[grand_child] == 1
    assert mcts.rewards[state] == 1
    assert mcts.rewards[child] == 1
    assert mcts.rewards[grand_child] == 1


def test_backpropagate_2():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=7,
        num_own_stones=[5, 5],
    )
    child = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=8,
        num_own_stones=[5, 5],
    )
    assert mcts.visits[state] == 0
    assert mcts.visits[child] == 0
    assert mcts.rewards[state] == 0
    assert mcts.rewards[child] == 0
    mcts.backpropagate(0, [state, child])
    assert mcts.visits[state] == 1
    assert mcts.visits[child] == 1
    assert mcts.rewards[state] == 0
    assert mcts.rewards[child] == 0
    mcts.backpropagate(0, [state, child])
    assert mcts.visits[state] == 2
    assert mcts.visits[child] == 2
    assert mcts.rewards[state] == 0
    assert mcts.rewards[child] == 0
    mcts.backpropagate(0.5, [state, child])
    assert mcts.visits[state] == 3
    assert mcts.visits[child] == 3
    assert mcts.rewards[state] == 0.5
    assert mcts.rewards[child] == 0.5
    mcts.backpropagate(1, [state, child])
    assert mcts.visits[state] == 4
    assert mcts.visits[child] == 4
    assert mcts.rewards[state] == 1.5
    assert mcts.rewards[child] == 1.5


def test_backpropagate_3():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [3, 3, 3, 3, 3, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=60,
        num_own_stones=[0, 0],
    )
    child = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [3, 3, 3, 3, 3, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [1, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=61,
        num_own_stones=[0, 0],
    )
    mcts.backpropagate(0, [state, child])
    assert mcts.visits[state] == 1
    assert mcts.visits[child] == 1
    assert mcts.rewards[state] == 0
    assert mcts.rewards[child] == 0


def test_rollout_1():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [2, 2, 2, 2, 1, 0, 1, 1],
                [3, 3, 3, 3, 3, 1, 0, 1],
                [2, 0, 0, 0, 0, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=17,
        num_own_stones=[0, 0],
    )
    child = engine.State(
        board=np.array(
            [
                [2, 2, 2, 2, 1, 0, 1, 1],
                [3, 3, 3, 3, 3, 1, 0, 1],
                [2, 0, 0, 0, 0, 1, 1, 0],
                [0, 0, 0, 1, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=18,
        num_own_stones=[0, 0],
    )
    assert len(mcts.unexpanded_moves[state]) == 47
    assert mcts.visits[state] == 0
    mcts.rollout(state)
    assert len(mcts.unexpanded_moves[state]) == 46
    assert len(mcts.children[state]) == 1
    assert mcts.visits[state] == 1
    assert mcts.rewards[state] == 0
    for _ in range(45):
        mcts.rollout(state)
    assert len(mcts.unexpanded_moves[state]) == 1
    assert len(mcts.children[state]) == 46
    mcts.rollout(state)
    assert mcts.visits[state] == 47
    assert mcts.rewards[state] == 0
    assert mcts.unexpanded_moves[state] == []
    assert len(mcts.children[state]) == 47
    assert child in mcts.children[state]
    mcts.rollout(state)
    assert mcts.visits[state] == 48
    assert mcts.rewards[state] == 0
    assert mcts.unexpanded_moves[state] == []
    assert len(mcts.children[state]) == 47
    for _ in range(100):
        mcts.rollout(state)
    assert mcts.visits[state] == 148
    assert mcts.rewards[state] == 0
    assert mcts.unexpanded_moves[state] == []
    assert len(mcts.children[state]) == 47


def test_rollout_2():
    mcts = MCTS.MCTS()
    state = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 0, 1, 1],
                [0, 0, 1, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=8,
        num_own_stones=[5, 5],
    )
    child = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 1, 1, 1],
                [0, 0, 1, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=9,
        num_own_stones=[5, 5],
    )

    for _ in range(150):
        mcts.rollout(state)
    assert child in mcts.children[state]


def test_move_1():
    mcts = MCTS.MCTS(time_for_move=0.1)
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=0,
        num_own_stones=[5, 5],
    )
    move = mcts.move(state)
    assert move in MCTS.find_moves(state)


def test_move_2():
    mcts = MCTS.MCTS(time_for_move=0.1)
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [1, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=7,
        num_own_stones=[5, 5],
    )
    move = mcts.move(state)
    assert move in MCTS.find_moves(state)


def test_bfs_1():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=0,
        num_own_stones=[5, 5],
    )
    dist_0 = np.array(
        [
            [8, 8, 8, 8, 8, 8, 8, 8],
            [7, 7, 7, 7, 7, 7, 7, 7],
            [6, 6, 6, 6, 6, 6, 6, 6],
            [5, 5, 5, 5, 5, 5, 5, 5],
            [4, 4, 4, 4, 4, 4, 4, 4],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [2, 2, 2, 2, 2, 2, 2, 2],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ],
        dtype=np.int8,
    )
    dist_1 = np.array(
        [
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
        ],
        dtype=np.int8,
    )
    actual_output = engine.bfs(state)
    assert np.array_equal(actual_output[0], dist_0)
    assert np.array_equal(actual_output[1], dist_1)


def test_bfs_2():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 2, 0, 0, 0, 0],
                [0, 0, 0, 0, 3, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=3,
        num_own_stones=[4, 4],
    )
    dist_0 = np.array(
        [
            [8, 8, 8, 7, 8, 8, 7, 8],
            [7, 7, 7, 6, 7, 7, 6, 7],
            [6, 6, 6, 5, 6, 6, 5, 6],
            [5, 5, 5, 4, 5, 5, 4, 5],
            [4, 4, 4, 4, 64, 4, 3, 4],
            [3, 3, 3, 3, 3, 3, 2, 3],
            [2, 2, 2, 2, 2, 2, 1, 2],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ],
        dtype=np.int8,
    )
    dist_1 = np.array(
        [
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [8, 7, 6, 64, 4, 3, 2, 1, 0],
            [7, 6, 5, 4, 3, 3, 2, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
            [7, 6, 5, 4, 3, 2, 1, 1, 0],
            [8, 7, 6, 5, 4, 3, 2, 1, 0],
        ],
        dtype=np.int8,
    )
    actual_output = engine.bfs(state)
    assert np.array_equal(actual_output[0], dist_0)
    assert np.array_equal(actual_output[1], dist_1)


def test_bfs_3():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 1, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0, 0, 0],
                [0, 2, 1, 0, 0, 0, 0, 0],
                [0, 2, 1, 0, 0, 0, 0, 0],
                [0, 2, 1, 0, 0, 0, 0, 0],
                [0, 2, 1, 0, 0, 0, 0, 0],
                [0, 2, 1, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=13,
        num_own_stones=[0, 5],
    )
    dist_0 = np.array(
        [
            [2, 1, 0, 1, 2, 3, 4, 5],
            [2, 1, 0, 1, 2, 3, 4, 5],
            [1, 0, 0, 1, 2, 3, 4, 5],
            [1, 0, 0, 1, 2, 3, 4, 5],
            [1, 0, 0, 1, 2, 3, 4, 4],
            [1, 0, 0, 1, 2, 3, 3, 3],
            [1, 0, 0, 1, 2, 2, 2, 2],
            [1, 1, 0, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ],
        dtype=np.int8,
    )
    dist_1 = np.array(
        [
            [7, 6, 5, 5, 4, 3, 2, 1, 0],
            [7, 6, 5, 5, 4, 3, 2, 1, 0],
            [8, 64, 5, 5, 4, 3, 2, 1, 0],
            [9, 64, 5, 5, 4, 3, 2, 1, 0],
            [10, 64, 5, 5, 4, 3, 2, 1, 0],
            [9, 64, 5, 5, 4, 3, 2, 1, 0],
            [8, 64, 5, 5, 4, 3, 2, 1, 0],
            [7, 6, 5, 5, 4, 3, 2, 1, 0],
        ],
        dtype=np.int8,
    )
    actual_output = engine.bfs(state)
    assert np.array_equal(actual_output[0], dist_0)
    assert np.array_equal(actual_output[1], dist_1)


def test_heur_evaluate_1():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 3, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=10,
        num_own_stones=[3, 3],
    )
    mcts = MCTS.MCTS_heuristic()
    assert mcts.evaluate(state) == 1


def test_heur_evaluate_2():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [1, 1, 3, 1, 1, 1, 1, 1],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 2, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=9,
        num_own_stones=[4, 4],
    )
    mcts = MCTS.MCTS_heuristic()
    assert mcts.evaluate(state) == 0


def test_extract_move_1():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 3, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=9,
        num_own_stones=[3, 3],
    )
    child_1 = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 3, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=10,
        num_own_stones=[3, 3],
    )
    child_2 = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 3, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=10,
        num_own_stones=[2, 3],
    )
    assert MCTS.extract_move(state, child_1) == (0, (3, 4))
    assert MCTS.extract_move(state, child_2) == (1, (3, 4))