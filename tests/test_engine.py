import sys

sys.path.append("..")

from src import engine
import numpy as np


def test_game_from_start_1():
    assert (
        engine.Game(
            engine.Test_Player(
                [
                    (0, (0, 1)),
                    (0, (0, 2)),
                    (0, (0, 3)),
                    (0, (0, 4)),
                    (0, (0, 5)),
                    (0, (0, 6)),
                    (0, (0, 7)),
                    (0, (0, 0)),
                    (0, (1, 1)),
                    (0, (5, 5)),
                ]
            ),
            engine.Test_Player(
                [
                    (0, (1, 0)),
                    (0, (2, 0)),
                    (0, (3, 0)),
                    (0, (4, 0)),
                    (0, (5, 0)),
                    (0, (6, 0)),
                    (0, (7, 0)),
                    (0, (0, 0)),
                    (0, (1, 1)),
                    (1, (0, 0)),
                ]
            ),
            False,
        ).play()
        == 0
    )


def test_game_from_start_2():
    assert (
        engine.Game(
            engine.Test_Player(
                [
                    (0, (0, 1)),
                    (1, (1, 2)),
                    (1, (1, 3)),
                    (1, (1, 4)),
                    (1, (1, 5)),
                    (1, (0, 6)),
                    (1, (0, 7)),
                    (0, (0, 0)),
                    (0, (1, 1)),
                    (0, (5, 5)),
                ]
            ),
            engine.Test_Player(
                [
                    (0, (1, 0)),
                    (0, (2, 0)),
                    (0, (3, 0)),
                    (0, (4, 0)),
                    (0, (5, 0)),
                    (0, (6, 0)),
                    (0, (7, 0)),
                    (0, (0, 0)),
                    (0, (1, 1)),
                    (1, (0, 0)),
                ]
            ),
            False,
        ).play()
        == 1
    )


def test_game_from_start_3():
    assert (
        engine.Game(
            engine.Test_Player(
                [
                    (0, (0, 0)),
                    (0, (1, 0)),
                    (1, (2, 0)),
                    (0, (3, 0)),
                ]
            ),
            engine.Test_Player(
                [
                    (0, (7, 0)),
                    (0, (6, 0)),
                    (0, (5, 0)),
                    (0, (4, 0)),
                ]
            ),
            False,
        ).play()
        == 1
    )


def test_game_from_start_4():
    assert (
        engine.Game(
            engine.Test_Player(
                [
                    (0, (0, 0)),
                    (0, (0, 1)),
                    (0, (0, 2)),
                    (0, (0, 3)),
                ]
            ),
            engine.Test_Player(
                [
                    (0, (0, 7)),
                    (0, (0, 6)),
                    (1, (0, 5)),
                    (0, (0, 4)),
                ]
            ),
            False,
        ).play()
        == 0
    )


def test_game_from_middle_1():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [1, 1, 1, 1, 0, 1, 1, 1],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=14,
        num_own_stones=[5, 5],
    )
    assert (
        engine.Game(
            player_1=engine.Test_Player(
                [
                    (0, (4, 4)),
                    (0, (0, 0)),
                ]
            ),
            player_2=engine.Test_Player(
                [
                    (1, (4, 4)),
                ]
            ),
            display_board=False,
            state=state,
        ).play()
        == 0
    )


def test_game_from_middle_2():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=6,
        num_own_stones=[4, 5],
    )
    assert (
        engine.Game(
            player_1=engine.Test_Player(
                [
                    (0, (7, 4)),
                ]
            ),
            player_2=engine.Test_Player(
                [
                    (0, (0, 4)),
                ]
            ),
            display_board=False,
            state=state,
        ).play()
        == 1
    )


def test_game_from_middle_3():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 3, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=6,
        num_own_stones=[5, 4],
    )
    assert (
        engine.Game(
            player_1=engine.Test_Player(
                [
                    (0, (4, 7)),
                ]
            ),
            player_2=engine.Test_Player(
                [
                    (0, (4, 0)),
                ]
            ),
            display_board=False,
            state=state,
        ).play()
        == 0
    )


def test_game_from_middle_4():
    state = engine.State(
        board=np.array(
            [
                [1, 1, 1, 1, 1, 2, 1, 0],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1, 2, 1, 1],
                [3, 3, 3, 3, 3, 0, 1, 1],
                [1, 1, 1, 1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1, 1, 1, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=59,
        num_own_stones=[0, 0],
    )
    game = engine.Game(player_1=None, player_2=None, display_board=False, state=state)
    assert game.is_valid((0, (0, 7)), False)
    assert game.is_valid((0, (6, 0)), False) == False
    assert game.is_valid((0, (6, 1)), False) == False
    assert game.is_valid((0, (6, 2)), False) == False
    assert game.is_valid((0, (6, 3)), False) == False
    assert game.is_valid((0, (6, 4)), False) == False
    assert game.is_valid((0, (6, 5)), False) == False
    assert game.is_valid((0, (6, 6)), False) == False
    assert game.is_valid((0, (6, 7)), False) == False
    assert game.is_valid((0, (7, 0)), False)
    assert game.is_valid((0, (5, 5)), False) == False
    assert game.is_valid((0, (6, 6)), False) == False
    assert game.is_valid((0, (7, 7)), False) == False


def test_game_from_end_1():
    state = engine.State(
        board=np.array(
            [
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 3, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 3, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 2, 0, 0, 0],
                [0, 0, 0, 0, 1, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=10,
        num_own_stones=[3, 3],
    )
    assert (
        engine.Game(
            player_1=engine.Test_Player([]),
            player_2=engine.Test_Player([]),
            display_board=False,
            state=state,
        ).play()
        == 1
    )


def test_game_from_end_2():
    state = engine.State(
        board=np.array(
            [[1, 1, 1, 1], [0, 0, 2, 0], [2, 2, 0, 0], [3, 3, 0, 1]], dtype=np.int8
        ),
        moves_count=10,
        num_own_stones=[0, 1],
    )
    assert (
        engine.Game(
            player_1=engine.Test_Player([]),
            player_2=engine.Test_Player([]),
            display_board=False,
            state=state,
        ).play()
        == 0
    )


def test_game_from_end_3():
    state = engine.State(
        board=np.array(
            [[3, 2, 1, 3], [0, 1, 3, 2], [1, 0, 1, 1], [2, 1, 1, 1]], dtype=np.int8
        ),
        moves_count=14,
        num_own_stones=[0, 0],
    )
    assert (
        engine.Game(
            player_1=engine.Test_Player([]),
            player_2=engine.Test_Player([]),
            display_board=False,
            state=state,
        ).play()
        == 0.5
    )


def test_state_hash_1():
    state_1 = engine.State(
        board=np.array(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=2,
        num_own_stones=[4, 5],
    )
    state_2 = engine.State(
        board=np.array(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=2,
        num_own_stones=[4, 5],
    )
    assert hash(state_1) == hash(state_2)


def test_state_hash_2():
    state_1 = engine.State(
        board=np.array(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=2,
        num_own_stones=[4, 5],
    )
    state_2 = engine.State(
        board=np.array(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 0, 0, 0, 0, 0, 0],
                [0, 0, 1, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0],
            ],
            dtype=np.int8,
        ),
        moves_count=2,
        num_own_stones=[4, 5],
    )
    assert hash(state_1) != hash(state_2)


def test_neighbours_1():
    board = np.array(
        [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ],
        dtype=np.int8,
    )
    assert engine.neighbours((0, 0), board) == [(1, 0), (0, 1)]
    assert engine.neighbours((7, 7), board) == [(6, 7), (7, 6)]
    assert engine.neighbours((5, 5), board) == [(4, 5), (6, 5), (5, 4), (5, 6)]


def test_history_1():
    game = engine.Game(
        player_1=engine.Test_Player(
            moves=[
                (0, (0, 0)),
                (1, (2, 0)),
            ]
        ),
        player_2=engine.Test_Player(
            moves=[
                (0, (1, 0)),
                (0, (0, 0)),
                (0, (3, 0)),
            ]
        ),
        display_board=False,
        state=engine.State(
            board_size=4,
            num_own_stones=[3, 3],
        ),
    )
    game.play()
    assert game.history == [
        (0, (0, 0)),
        (0, (1, 0)),
        (1, (2, 0)),
        (0, (3, 0)),
    ]
