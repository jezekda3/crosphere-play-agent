import sys

sys.path.append("..")

import math
import time
import random
import copy
import numpy as np
from collections import defaultdict
import torch
import torch.nn as nn
from . import engine

# random.seed(10)


def pop_random(input_list):
    return input_list.pop(random.randint(0, len(input_list) - 1))


def find_moves(state):
    board_size = state.board.shape[0]
    game = engine.Game(
        player_1=None,
        player_2=None,
        display_board=False,
        state=state,
    )
    if game.state.game_finished:
        return []
    moves = []
    stone_types = [0]
    if state.num_own_stones[state.moves_count % 2] >= 1:
        stone_types = [0, 1]
    for stone_type in stone_types:
        for i in range(board_size):
            for j in range(board_size):
                move = (stone_type, (i, j))
                if game.is_valid(move, False):
                    moves.append(move)
    return moves


def extract_move(parent, child):
    for i in range(parent.board_size):
        for j in range(parent.board_size):
            if parent.board[i, j] != child.board[i, j]:
                return (int(child.board[i, j] > 1), (i, j))


class Random_Player(engine.Player):
    def move(self, state):
        # return pop_random(find_moves(state))
        return (
            random.randint(0, 1),
            (
                random.randint(0, state.board_size - 1),
                random.randint(0, state.board_size - 1),
            ),
        )


class Default_Moves_Dict(dict):
    def __missing__(self, key):
        self[key] = find_moves(key)
        return self[key]


class MCTS(engine.Player):
    def __init__(
        self, exploration_weight=1, time_for_move=None, rollouts_per_move=None
    ):
        self.exploration_weight = exploration_weight
        self.time_for_move = time_for_move
        self.rollouts_per_move = rollouts_per_move
        self.rewards = defaultdict(int)
        self.visits = defaultdict(int)
        self.children = defaultdict(list)
        self.unexpanded_moves = Default_Moves_Dict()
        self.start_time = None

    def __repr__(self):
        mcts_repr = (
            engine.Player.__repr__(self)
            + ", "
            + "exploration_weight="
            + str(self.exploration_weight)
        )
        if self.time_for_move is not None:
            mcts_repr += ", " + "time_for_move=" + str(self.time_for_move)
        if self.rollouts_per_move is not None:
            mcts_repr += ", " + "rollouts_per_move=" + str(self.rollouts_per_move)
        return mcts_repr

    def __deepcopy__(self, memo=None):
        new_mcts = MCTS(
            self.exploration_weight, self.time_for_move, self.rollouts_per_move
        )
        new_mcts.rewards = copy.deepcopy(self.rewards)
        new_mcts.visits = copy.deepcopy(self.visits)
        new_mcts.children = copy.deepcopy(self.children)
        new_mcts.unexpanded_moves = copy.deepcopy(self.unexpanded_moves)
        new_mcts.start_time = copy.deepcopy(self.start_time)
        return new_mcts

    def relative_reward(self, child):
        return abs(
            ((child.moves_count + 1) % 2) * self.visits[child] - self.rewards[child]
        )

    def ucb_score(self, node, child):
        w = self.relative_reward(child)
        n = self.visits[child]
        c = self.exploration_weight
        t = self.visits[node]
        return w / n + c * math.sqrt(math.log(t) / n)

    def best_score(self, node, child):
        return self.relative_reward(child) / self.visits[child]

    def choose(self, node, score_metric):
        children = self.children[node]
        best_score = 0
        best_child = children[0]
        for child in children:
            score = score_metric(node, child)
            if score > best_score:
                best_score = score
                best_child = child
        return best_child

    def select(self, input_node):
        node = input_node
        path = [node]
        while not self.unexpanded_moves[node]:
            if not self.children[node]:
                return path
            node = self.choose(node, self.ucb_score)
            path.append(node)
        return path

    def create_child(self, node, move):
        new_stone, new_pos = move
        new_child = copy.deepcopy(node)
        new_child.board[new_pos] = engine.convert_stone(new_stone, node.moves_count)
        new_child.moves_count += 1
        if new_stone == 1:
            new_child.num_own_stones[node.moves_count % 2] -= 1
        self.children[node].append(new_child)
        return new_child

    def expand(self, path):
        node = path[-1]
        if not self.unexpanded_moves[node]:
            return
        new_move = pop_random(self.unexpanded_moves[node])
        path.append(self.create_child(node, new_move))

    def evaluate(self, state):
        reward = engine.Game(
            player_1=Random_Player(),
            player_2=Random_Player(),
            display_board=False,
            state=copy.deepcopy(state),
        ).play()
        return reward

    def backpropagate(self, reward, path):
        while path:
            node = path.pop(0)
            self.visits[node] += 1
            self.rewards[node] += reward

    def rollout(self, node):
        path = self.select(node)
        self.expand(path)
        reward = self.evaluate(path[-1])
        self.backpropagate(reward, path)

    def time_is_up(self):
        if self.time_for_move is None:
            return False
        return time.time() - self.start_time >= self.time_for_move

    def rollouts_are_up(self, rollouts_completed):
        if self.rollouts_per_move is None:
            return False
        return rollouts_completed >= self.rollouts_per_move

    def move(self, state):
        memory_usage = get_memory_usage()
        print("Memory usage:", memory_usage, "bytes")
        node = copy.deepcopy(state)
        rollouts_completed = 0
        self.start_time = time.time()
        while True:
            self.rollout(node)
            rollouts_completed += 1
            if self.time_is_up() or self.rollouts_are_up(rollouts_completed):
                break
        print("Rollouts completed: ", rollouts_completed)
        chosen_state = self.choose(node, self.best_score)
        return extract_move(state, chosen_state)


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


class MCTS_heuristic(MCTS):
    def __init__(
        self,
        exploration_weight=1,
        time_for_move=None,
        rollouts_per_move=None,
        stone_weight=0.5,
    ):
        MCTS.__init__(self, exploration_weight, time_for_move, rollouts_per_move)
        self.stone_weight = stone_weight

    def __deepcopy__(self, memo=None):
        new_mcts = MCTS_heuristic(
            self.exploration_weight, self.time_for_move, self.rollouts_per_move
        )
        new_mcts.rewards = copy.deepcopy(self.rewards)
        new_mcts.visits = copy.deepcopy(self.visits)
        new_mcts.children = copy.deepcopy(self.children)
        new_mcts.unexpanded_moves = copy.deepcopy(self.unexpanded_moves)
        new_mcts.start_time = copy.deepcopy(self.start_time)
        new_mcts.stone_weight = self.stone_weight
        return new_mcts

    def __repr__(self):
        return MCTS.__repr__(self) + ", " + "stone_weight=" + str(self.stone_weight)

    def evaluate(self, state):
        distances = engine.bfs(state)
        pl_1_dists = distances[0][0]
        pl_2_dists = [row[0] for row in distances[1]]
        pl_1_dist = min(pl_1_dists)
        pl_2_dist = min(pl_2_dists)
        if pl_1_dist == 0 and pl_2_dist != 0:
            return 1
        if pl_2_dist == 0 and pl_1_dist != 0:
            return 0
        pl_1_stones = state.num_own_stones[0]
        pl_2_stones = state.num_own_stones[1]
        reward = sigmoid(
            pl_2_dist - pl_1_dist + self.stone_weight * (pl_1_stones - pl_2_stones)
        )

        memory_usage = get_memory_usage()
        #print("Memory usage heur:", memory_usage, "bytes")
        return reward


import psutil

# Function to get memory usage of the current process
def get_memory_usage():
    process = psutil.Process()
    mem_info = process.memory_info()
    return mem_info.rss  # Return memory usage in bytes


class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv3d(
            in_channels=1, out_channels=32, kernel_size=(3, 3, 3), padding=(1, 1, 1)
        )
        self.conv2 = nn.Conv3d(
            in_channels=32, out_channels=64, kernel_size=(3, 3, 3), padding=(1, 1, 1)
        )
        self.fc1 = nn.Linear(64 * 8 * 8 * 11, 128)
        self.fc2 = nn.Linear(128, 1)

    def forward(self, x):
        x = x.unsqueeze(1)  # Add channel dimension
        x = torch.relu(self.conv1(x))
        x = torch.relu(self.conv2(x))
        x = x.view(-1, 64 * 8 * 8 * 11)  # Flatten
        x = torch.relu(self.fc1(x))
        x = self.fc2(x)
        return x

#class CNN(nn.Module):
#    def __init__(self):
#        super(CNN, self).__init__()
#        self.conv1 = nn.Conv3d(in_channels=1, out_channels=32, kernel_size=(3, 3, 3), padding=(1, 1, 1))
#        self.conv2 = nn.Conv3d(in_channels=32, out_channels=64, kernel_size=(3, 3, 3), padding=(1, 1, 1))
#        self.conv3 = nn.Conv3d(in_channels=64, out_channels=128, kernel_size=(3, 3, 3), padding=(1, 1, 1))
#        self.conv4 = nn.Conv3d(in_channels=128, out_channels=256, kernel_size=(3, 3, 3), padding=(1, 1, 1))
#        self.fc1 = nn.Linear(256 * 8 * 8 * 11, 512)
#        self.fc2 = nn.Linear(512, 256)
#        self.fc3 = nn.Linear(256, 1)
#
#    def forward(self, x):
#        x = x.unsqueeze(1)  # Add channel dimension
#        x = torch.relu(self.conv1(x))
#        x = torch.relu(self.conv2(x))
#        x = torch.relu(self.conv3(x))
#        x = torch.relu(self.conv4(x))
#        x = x.view(-1, 256 * 8 * 8 * 11)  # Flatten
#        x = torch.relu(self.fc1(x))
#        x = torch.relu(self.fc2(x))
#        x = self.fc3(x)
#        return x
    
def load_model(model_path):
        model = CNN()  # Assuming CNN class is defined as before
        model.load_state_dict(torch.load(model_path))
        model.eval()
        return model

class MCTS_cnn(MCTS):
    def __init__(
        self,
        exploration_weight=1,
        time_for_move=None,
        rollouts_per_move=None,
        model_path=None,
    ):
        MCTS.__init__(self, exploration_weight, time_for_move, rollouts_per_move)
        self.model_path = model_path
        self.model = load_model(model_path)
        memory_usage = get_memory_usage()
        #print("Memory usage cnn init          :", memory_usage, "bytes")

    def __deepcopy__(self, memo=None):
        new_mcts = MCTS_cnn(
            self.exploration_weight,
            self.time_for_move,
            self.rollouts_per_move,
            self.model_path,
        )
        new_mcts.rewards = copy.deepcopy(self.rewards)
        new_mcts.visits = copy.deepcopy(self.visits)
        new_mcts.children = copy.deepcopy(self.children)
        new_mcts.unexpanded_moves = copy.deepcopy(self.unexpanded_moves)
        new_mcts.start_time = copy.deepcopy(self.start_time)
        memory_usage = get_memory_usage()
        #print("Memory usage cnn deepcopy      :", memory_usage, "bytes")
        return new_mcts

    def __repr__(self):
        return MCTS.__repr__(self) + ", " + "model_path=" + str(self.model_path)

    def evaluate(self, state):
        memory_usage = get_memory_usage()
        #print("Memory usage cnn start evaluate:", memory_usage, "bytes")
        input_data = engine.preprocess_state(state)
        pl_1_dist = input_data[6, 0, 0]
        pl_2_dist = input_data[7, 0, 0]

        if pl_1_dist == 0 and pl_2_dist != 0:
            return 1
        if pl_2_dist == 0 and pl_1_dist != 0:
            return 0

        memory_usage = get_memory_usage()
        #print("Memory usage cnn after preproce:", memory_usage, "bytes")

        input_tensor = torch.tensor(input_data).unsqueeze(0)  # Add batch dimension
        
        memory_usage = get_memory_usage()
        #print("Memory usage cnn after tensor_c:", memory_usage, "bytes")

        with torch.no_grad():
            prediction = self.model(input_tensor.float()).item()

        #memory_usage = get_memory_usage()
        #print("Memory usage cnn end   evaluate:", memory_usage, "bytes")
        return prediction
