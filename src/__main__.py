import sys

sys.path.append("..")

from . import engine
from . import MCTS
import numpy as np
import random


def main():
    game = engine.Game(
        player_1=MCTS.MCTS_heuristic(
            exploration_weight=0.4,
            time_for_move=2,
            stone_weight=0.25,
        ),
        player_2=MCTS.MCTS_heuristic(
            exploration_weight=0.3,
            time_for_move=2,
            stone_weight=0.4,
        ),
    )
    game.play()
    # game.save_results("data/games/local_games.txt")

    # tournament = engine.Tournament(
    #    players=[
    #        MCTS.Random_Player(),
    #        MCTS.MCTS(
    #           exploration_weight=0.4,
    #           time_for_move=1,
    #       ),
    #        MCTS.MCTS_heuristic(
    #           exploration_weight=0.3,
    #           time_for_move=1,
    #           stone_weight=0.25,
    #       ),
    #        MCTS.MCTS_cnn(
    #           exploration_weight=0.3,
    #           time_for_move=1,
    #           model_path="model_5_195.pth"
    #       )
    #    ],
    #    num_games=100,
    #    games_file_path="data/games/local_games.txt",
    # )
    # tournament.run()
    # tournament.save_results(file_path="data/tournaments/tournaments.txt")
    # print(tournament.get_results())

    # while True:
    #    explore_1 = round(random.uniform(0.15, 0.5), 2)
    #    explore_2 = round(random.uniform(0.15, 0.5), 2)
    #    rolls_1 = random.randint(500, 2500)
    #    rolls_2 = random.randint(500, 2500)
    #    stone_1 = round(random.uniform(0.25, 0.55), 2)
    #    stone_2 = round(random.uniform(0.25, 0.55), 2)
    #    game = engine.Game(
    #        player_1=MCTS.MCTS_heuristic(
    #            exploration_weight=explore_1,
    #            rollouts_per_move=rolls_1,
    #            stone_weight=stone_1,
    #        ),
    #        player_2=MCTS.MCTS_heuristic(
    #            exploration_weight=explore_2,
    #            rollouts_per_move=rolls_2,
    #            stone_weight=stone_2,
    #        ),
    #    )
    #    game.play()
    #    game.save_results("data/games/local_games.txt")


if __name__ == "__main__":
    main()


# taskset -c 0 python3 -m src.crosphere_play_agent.__main__ &
# taskset -c 1 python3 -m src.crosphere_play_agent.__main__ &
# taskset -c 2 python3 -m src.crosphere_play_agent.__main__ &
# taskset -c 3 python3 -m src.crosphere_play_agent.__main__ &
