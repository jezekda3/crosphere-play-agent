import numpy as np
import copy
import random
from datetime import datetime
import platform
import re


def split_alternating(lst):
    list1 = lst[::2]  # Start from the first element, get every second element
    list2 = lst[1::2]  # Start from the second element, get every second element
    return list1, list2


def device_info():
    system_info = platform.system()
    node_info = platform.node()
    return f"system={system_info}, node={node_info}"


def bfs(state):
    opened = [[], []]
    closed = [[], []]
    distances = [
        np.full((state.board_size + 1, state.board_size), 64, dtype=np.int8),
        np.full((state.board_size, state.board_size + 1), 64, dtype=np.int8),
    ]

    for i in range(state.board_size):
        opened[0].append((state.board_size, i))
        opened[1].append((i, state.board_size))
        distances[0][state.board_size, i] = 0
        distances[1][i, state.board_size] = 0

    for i in [0, 1]:
        while opened[i]:
            current = opened[i].pop(0)
            for neighbour in neighbours(current, state.board):
                if (
                    neighbour not in opened[i]
                    and neighbour not in closed[i]
                    and state.board[neighbour] != 2 + abs(i - 1)
                ):
                    if state.board[neighbour] == 0:
                        opened[i].append(neighbour)
                        distances[i][neighbour] = distances[i][current] + 1
                    else:
                        opened[i].insert(0, neighbour)
                        distances[i][neighbour] = distances[i][current]
            closed[i].append(current)
    return distances


def preprocess_state(state):
    distances = bfs(state)
    distances[0] = distances[0][: state.board_size]
    distances[1] = distances[1][:, : state.board_size]
    pl_1_dists = distances[0][0]
    pl_2_dists = [row[0] for row in distances[1]]
    pl_1_dist = min(pl_1_dists)
    pl_2_dist = min(pl_2_dists)
    empty_fields = np.where(state.board == 0, 1, 0)
    common_stones = np.where(state.board == 1, 1, 0)
    own_stones_1 = np.where(state.board == 2, 1, 0)
    own_stones_2 = np.where(state.board == 3, 1, 0)
    return np.array(
        [
            empty_fields,  # Game board as one-hot encoding
            common_stones,
            own_stones_1,
            own_stones_2,
            distances[0],  # closest distance from the point to the bottom edge
            distances[1],  # closest distance from the point to the right edge
            np.full(
                (state.board_size, state.board_size),
                pl_1_dist,
            ),  # minimum number of moves for player_1 to win
            np.full(
                (state.board_size, state.board_size),
                pl_2_dist,
            ),  # minimum number of moves for player_2 to win
            np.full(
                (state.board_size, state.board_size),
                state.moves_count % 2,
            ),  # whether current player is player_1 (0/1)
            np.full(
                (state.board_size, state.board_size),
                state.num_own_stones[0],
            ),  # number of own stones of the player_1
            np.full(
                (state.board_size, state.board_size),
                state.num_own_stones[1],
            ),  # number of own stones of the player_2
        ],
        dtype=np.int8,
    )

def preprocess_state_basic(state):
    empty_fields = np.where(state.board == 0, 1, 0)
    common_stones = np.where(state.board == 1, 1, 0)
    own_stones_1 = np.where(state.board == 2, 1, 0)
    own_stones_2 = np.where(state.board == 3, 1, 0)
    return np.array(
        [
            empty_fields,  # Game board as one-hot encoding
            common_stones,
            own_stones_1,
            own_stones_2,
        ],
        dtype=np.int8,
    )


def preprocess_games(input_filepath, output_filepath):
    games_data = upload_games(input_filepath)
    data = []
    for game_data in games_data:
        game_moves, game_result = game_data
        moves_1, moves_2 = split_alternating(game_moves)
        game = Game(Test_Player(moves_1), Test_Player(moves_2), display_board=False)
        #random_move_index = random.randint(0, len(game_moves) - 1)
        for move_index in range(len(game_moves)):
            game.play(move_index)
            block = preprocess_state(game.state)
            result_array = (
                np.full(
                    (game.state.board_size, game.state.board_size),
                    game_result, dtype=np.int8
                ),
            )
            block_with_result = np.concatenate(
                (block, result_array), axis=0
            )
            data.append(block_with_result)
    np.save(output_filepath, data)


def upload_game(lines):
    moves = []
    result = None
    for line in lines:
        if line.endswith(")\n"):
            move = line.split(".", 1)[1]
            parts = [int(char) for char in move if char.isdigit()]
            stone = int(parts[0])
            position = (int(parts[1]), int(parts[2]))
            moves.append((stone, position))
        elif line.startswith("[Result"):
            result = float(re.search(r"\d+(\.\d+)?", line).group())
    return (moves, result)


def upload_games(file_name):
    games = []
    with open(file_name, "r") as file:
        lines = file.readlines()
    indexes = []
    for i, line in enumerate(lines[1:]):
        if line.startswith("[Player_1"):
            indexes.append(i)
    splitted = []
    start = 0
    for index in indexes:
        splitted.append(lines[start:index])
        start = index
    splitted.append(lines[start:])  # Append the last segment of the list
    for game in splitted:
        games.append(upload_game(game))
    return games


def tag(name, content):
    return f'[{name} "{content}"]\n'


colors = {
    0: "\033[90m",  # Black: empty field
    1: "\033[93m",  # White: common stone
    2: "\033[94m",  # Blue:  1st player's stone
    3: "\033[92m",  # Green: 2nd player's stone
}


def str_board(board, colored=True):
    str_board = "  "
    for i in range(board.shape[0]):
        str_board += str(i) + " "
    str_board += "\n"
    for i, row in enumerate(board):
        str_board += str(i) + " "
        for cell in row:
            if colored:
                str_board += f"{colors[cell]}{cell}\033[0m "
            else:
                str_board += f"{cell} "
        str_board += "\n"
    return str_board


class Player:
    def __repr__(self):
        return "type=" + self.__class__.__name__


class Human(Player):
    def move(self, state):
        while True:
            input_line = input("Your move: ")
            try:
                input_nums = list(map(int, input_line.split()))
                if len(input_nums) == 3:
                    stone_type, pos_x, pos_y = input_nums
                    return (stone_type, (pos_x, pos_y))
            except ValueError:
                pass


class Test_Player(Player):
    __test__ = False

    def __init__(self, moves):
        self.moves = moves
        self.moves_count = 0

    def move(self, board):
        self.moves_count += 1
        return self.moves[self.moves_count - 1]


class State:
    def __init__(
        self,
        board_size=8,
        num_own_stones=None,
        board=None,
        moves_count=0,
    ):
        if board is None:
            board = np.zeros((board_size, board_size), dtype=np.int8)
        if num_own_stones is None:
            num_own_stones = [5, 5]
        self.board = board
        self.moves_count = moves_count
        self.num_own_stones = num_own_stones
        self.board_size = self.board.shape[0]
        self.game_finished = False
        self.won = [False, False]

    def __hash__(self):
        return hash(tuple(self.board.flatten()))

    def __str__(self):
        return (
            f"{str_board(self.board)}"
            f"Moves Played: {self.moves_count}\n"
            f"On turn: Player {self.moves_count%2+1}\n"
            f"Number of Own Stones: {self.num_own_stones}\n"
            f"Game Finished: {self.game_finished}\n"
            f"Won: {self.won}\n"
        )

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if not isinstance(other, State):
            return False
        if (
            np.array_equal(self.board, other.board)
            and self.moves_count == other.moves_count
            and self.num_own_stones == other.num_own_stones
        ):
            return True
        return False

    def __deepcopy__(self, memo=None):
        new_state = State(
            board=np.copy(self.board),
            moves_count=self.moves_count,
            num_own_stones=self.num_own_stones.copy(),
        )
        new_state.board_size = self.board_size
        new_state.game_finished = self.game_finished
        new_state.won = self.won.copy()
        return new_state


def convert_stone(stone_type, moves_count):
    if stone_type == 1:
        if moves_count % 2 == 1:
            return 3
        return 2
    return 1


def neighbours(pos, board):
    pos_x, pos_y = pos
    neighbours = []
    if pos_x > 0:
        neighbours.append((pos_x - 1, pos_y))
    if pos_x < board.shape[0] - 1:
        neighbours.append((pos_x + 1, pos_y))
    if pos_y > 0:
        neighbours.append((pos_x, pos_y - 1))
    if pos_y < board.shape[1] - 1:
        neighbours.append((pos_x, pos_y + 1))
    return neighbours


class Game:
    def __init__(
        self,
        player_1=None,
        player_2=None,
        display_board=True,
        state=None,
    ):
        if state is None:
            state = State()
        if player_1 is None:
            player_1 = Human()
        if player_2 is None:
            player_2 = Human()
        self.state = state
        self.player_1 = player_1
        self.player_2 = player_2
        self.display_board = display_board
        self.opened = [[], []]
        self.closed = [[], []]
        self.history = []
        for i in range(self.state.board_size):
            if self.state.board[0][i] == 1 or self.state.board[0][i] == 2:
                self.opened[0].append((0, i))
            if self.state.board[i][0] == 1 or self.state.board[i][0] == 3:
                self.opened[1].append((i, 0))
        self.bfs([0, 0])

    def __repr__(self):
        game_repr = (
            tag("Player_1", repr(self.player_1))
            + tag("Player_2", repr(self.player_2))
            + tag("Result", self.get_result())
            + tag("Datetime", datetime.now().strftime("%d-%m-%Y %H:%M:%S"))
            + tag("Device", device_info())
            + "\n"
        )
        for i, move in enumerate(self.history, 1):
            game_repr += f"{i}. {move}\n"
        return game_repr + "\n\n"

    def save_results(self, file_path):
        with open(file_path, "a") as file:
            file.write(repr(self))

    def check_connection(self, pos):
        num_inserted = [0, 0]
        for i in [0, 1]:
            if self.state.board[pos] == 1 or self.state.board[pos] == 2 + i:
                if pos[i] == 0:
                    self.opened[i].append(pos)
                    num_inserted[i] += 1
                if pos not in self.opened[i]:
                    for neighbour in neighbours(pos, self.state.board):
                        if neighbour in self.closed[i] or neighbour in self.opened[i]:
                            self.opened[i].append(pos)
                            num_inserted[i] += 1
                            break
        return num_inserted

    def bfs(self, num_inserted):
        for i in [0, 1]:
            while self.opened[i]:
                current = self.opened[i].pop(0)
                if current[i] == self.state.board_size - 1:
                    self.state.game_finished = True
                    self.state.won[i] = True
                    # break
                for neighbour in neighbours(current, self.state.board):
                    if (
                        neighbour not in self.opened[i]
                        and neighbour not in self.closed[i]
                        and (
                            self.state.board[neighbour] == 1
                            or self.state.board[neighbour] == 2 + i
                        )
                    ):
                        self.opened[i].append(neighbour)
                        num_inserted[i] += 1
                self.closed[i].append(current)
        return num_inserted

    def is_valid(self, move, modify=True):
        stone_type, pos = move

        # Check indexes
        pos_x, pos_y = pos
        board_size = self.state.board_size
        if (
            pos_x < 0
            or pos_x >= board_size
            or pos_y < 0
            or pos_y >= board_size
            or stone_type < 0
            or stone_type > 1
        ):
            # print("Index out of board!")
            return False

        # Check if the player has enough own stones
        if stone_type == 1:
            if self.state.num_own_stones[self.state.moves_count % 2] <= 0:
                # print("You do not have enough your own stones!")
                return False

        # Check if the square is free
        if self.state.board[pos] != 0:
            # print("This square is not free!")
            return False

        self.state.board[pos] = convert_stone(stone_type, self.state.moves_count)
        num_inserted = self.bfs(self.check_connection(pos))
        self.state.board[pos] = 0

        # Check if the move does not result in a simultaneous
        # win for both sides
        both_win = self.state.won[0] and self.state.won[1]
        if not modify or both_win:
            self.state.won[0] = False
            self.state.won[1] = False
            self.state.game_finished = False
            if num_inserted[0] > 0:
                self.closed[0] = self.closed[0][: -num_inserted[0]]
            if num_inserted[1] > 0:
                self.closed[1] = self.closed[1][: -num_inserted[1]]

        if both_win:
            # print("Both players would win simultaniously!")
            return False
        # print("Everything OK")
        return True

    def get_result(self):
        if self.state.won[0] == 1 and self.state.won[1] == 0:
            return 1
        if self.state.won[0] == 0 and self.state.won[1] == 1:
            return 0
        if self.state.won[0] == 0 and self.state.won[1] == 0:
            return 0.5
        return "Game not yet finished"

    def is_draw(self):
        if self.state.num_own_stones[self.state.moves_count % 2] >= 1:
            return False
        for i in range(self.state.board_size):
            for j in range(self.state.board_size):
                move = (0, (i, j))
                if self.is_valid(move, False):
                    return False
        self.state.game_finished = True
        return True

    def play(self, max_moves=64):
        while (
            not self.state.game_finished
            and not self.is_draw()
            and self.state.moves_count < max_moves
        ):
            player_on_turn = self.player_1
            if self.state.moves_count % 2 == 1:
                player_on_turn = self.player_2
            if self.display_board:
                print(self.state)
            while True:
                move = player_on_turn.move(self.state)
                if self.is_valid(move):
                    stone_type, pos = move
                    if stone_type == 1:
                        self.state.num_own_stones[self.state.moves_count % 2] -= 1
                    self.state.board[pos] = convert_stone(
                        stone_type, self.state.moves_count
                    )
                    self.history.append(move)
                    break
            self.state.moves_count += 1
        if self.display_board:
            print(self.state)
        return self.get_result()


class Tournament:
    def __init__(
        self,
        players=None,
        num_games=10,
        board_size=8,
        num_own_stones=None,
        games_file_path=None,
        random_first=True,
    ):
        if players is None:
            players = []
        self.players = players
        self.num_players = len(players)
        self.num_games = num_games
        self.board_size = board_size
        if num_own_stones is None:
            num_own_stones = [5, 5]
        self.num_own_stones = num_own_stones
        self.games_file_path = games_file_path
        self.random_first = random_first
        self.results = np.zeros((self.num_players, self.num_players, 3))
        self.games_total = int(
            self.num_players * (self.num_players - 1) * num_games / 2
        )
        self.games_done = 0

    def save_results(self, file_path):
        with open(file_path, "a") as file:
            file.write(self.get_results())

    def run(self):
        for i, player_1 in enumerate(self.players):
            for j, player_2 in enumerate(self.players):
                if j >= i:
                    continue
                for _ in range(self.num_games):
                    first_player = None
                    second_player = None
                    reverse = 0
                    if self.random_first:
                        reverse = random.randint(0, 1)
                    if reverse == 0:
                        first_player = player_1
                        second_player = player_2
                    else:
                        first_player = player_2
                        second_player = player_1
                    game = Game(
                        player_1=copy.deepcopy(first_player),
                        player_2=copy.deepcopy(second_player),
                        state=State(
                            board_size=self.board_size,
                            num_own_stones=copy.deepcopy(self.num_own_stones),
                        ),
                        display_board=False,
                    )
                    result = game.play()
                    self.games_done += 1
                    print(str(self.games_done) + "/" + str(self.games_total))
                    if self.games_file_path is not None:
                        game.save_results(file_path=self.games_file_path)
                    self.results[j, i, int(abs(reverse - result) * 2)] += 1
                    self.results[i, j, int(2 - abs(reverse - result) * 2)] += 1

    def get_results(self):
        tour_repr = ""
        for i, player in enumerate(self.players, 1):
            tour_repr += tag(f"Player_{i}", repr(player))
        tour_repr += tag("Datetime", datetime.now().strftime("%d-%m-%Y %H:%M:%S"))
        tour_repr += tag("Device", device_info())
        tour_repr += "\n"
        abs_results = []
        first_row = []
        perc_results = []
        first_row.append("")
        for i in range(self.num_players):
            first_row.append(str(i + 1))
        abs_results.append(first_row)
        perc_results.append(first_row)
        for i, row in enumerate(self.results):
            new_abs_row = []
            new_perc_row = []
            new_abs_row.append(str(i + 1))
            new_perc_row.append(str(i + 1))
            for j, cell in enumerate(row):
                if i == j:
                    new_abs_row.append("-")
                    new_perc_row.append("-")
                else:
                    new_abs_row.append(
                        str(int(self.results[i, j, 0]))
                        + ":"
                        + str(int(self.results[i, j, 1]))
                        + ":"
                        + str(int(self.results[i, j, 2]))
                    )
                    perc_value = (
                        (self.results[i, j, 0] + self.results[i, j, 1] / 2)
                        / self.num_games
                        * 100
                    )
                    new_perc_row.append(str(round(perc_value, 1)) + "%")
            abs_results.append(new_abs_row)
            perc_results.append(new_perc_row)
        for row in abs_results:
            tour_repr += " ".join("{: ^8}".format(item) for item in row) + "\n"
        tour_repr += "\n"
        for row in perc_results:
            tour_repr += " ".join("{: ^8}".format(item) for item in row) + "\n"
        return tour_repr + "\n"
